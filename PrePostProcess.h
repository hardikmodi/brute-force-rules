/* 
 * File:   preprocess.h
 * Author: aeon
 *
 * Created on 10 April, 2013, 10:01 PM
 */

#ifndef PREPROCESS_H
#define	PREPROCESS_H
#include<string>
#include<map>

using namespace std;

class PrePostProcess {
public:
    
    PrePostProcess(const char* filepath,const char delim);    
    char* preProcess();
    char* postProcess(const char* rfname);
    char* getMapFile(void);
    virtual ~PrePostProcess();    
private:        
    char delimiter;
    char* fname;    
    char* ofname;
    char* mapfname;    
    char* resfname;
    
};

#endif	/* PREPROCESS_H */


