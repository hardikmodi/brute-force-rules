/* 
 * File:   subsetGen.h
 * Author: aeon
 *
 * Created on 25 January, 2013, 11:29 PM
 */

#ifndef SUBSETGEN_H
#define	SUBSETGEN_H

#include <set>
using namespace std;

class subsetGen {
private:
    int n;
    int current;
    int last;
    int * ele;
    int * ptrs;
    
public:
    set<int> buf;
    subsetGen(set<int> & s);
    void getNextSubset(void);
    virtual ~subsetGen();
private:

};

#endif	/* SUBSETGEN_H */

