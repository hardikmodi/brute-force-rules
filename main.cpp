/* 
 * File:   main.cpp
 * Author: aeon
 *
 * Created on 19 December, 2012, 9:45 PM
 */

/*
 * Brute Force algorithm to mine MUTUAL EXCLUSION RULES.
 * this algorithms first generates all the rules, calculates each rules support. 
 * if it is above a threshold it goes further to check the confidence 
 * if it is also above confidence threshold. It writes them to a file
 */

#include<stdio.h>
#include <cstdlib>
#include <iostream>
#include "Dataset.h"
#include "SetOfSet.h"
#include "BruteMiner.h"
#include "time.h"
#include <sstream>
#include <string>
#include <string.h>
#include "PrePostProcess.h"
#define max_filepath 500
using namespace std;

void generate_outpath(char *outfilepath,char *filepath,float supp_thres,float conf_thres)
{
        char *p;
        const char *q;
        stringstream ss(stringstream::in | stringstream::out);
        string val;
        strcpy(outfilepath,filepath);
        p=strrchr(outfilepath,'/');
        p++;
        *p='\0';
        strcat(outfilepath,"bf_rule_output_");
        ss<<supp_thres<<"_"<<conf_thres;
        val=ss.str();
        q=val.c_str();
        strcat(outfilepath,q);
        strcat(outfilepath,".txt");
}
void showTop10(char * of)
{
    char * cmd;
    cmd = new char[max_filepath];
    printf("TOP 10 RULES\n\n");
    strcpy(cmd,"cat ");
    strcat(cmd,of);
    strcat(cmd," | tr '\t' ':' | sort -k2 -t':' -r > sorted_temp.txt");
    system(cmd);        
    strcpy(cmd,"mv sorted_temp.txt ");
    strcat(cmd,of);
    system(cmd);
    strcpy(cmd,"head -n 10 ");
    strcat(cmd,of);
    system(cmd);
}

int main(void) 
{
    float supp_thres,conf_thres;
    long int merCount;
    char filepath[max_filepath],outfilepath[max_filepath]; 
    char *ifile,*resultFile;
    cout<<"Path of Dataset (use forward slash): ";
    cin>>filepath;                   
    cout<<"Support Threshold: ";
    cin>>supp_thres;
    cout<<"Confidence Threshold: ";
    cin>>conf_thres;
    generate_outpath(outfilepath,filepath,supp_thres,conf_thres);    
    clock_t start,end;        
    start=clock();
    PrePostProcess p(filepath,'\t');
    ifile=p.preProcess(); 
    Dataset D1(ifile,'\t');
    D1.summarize();
    BruteMiner miner1(D1);
    merCount = miner1.mine(outfilepath,supp_thres,conf_thres);    
    end=clock();
    printf("\n\nAlgo\t|#Items\t| #DS \t| Supp \t| Conf \t| #Rules \t| P1 Time(sec) \t| P1 Clock Ticks \t| Time(sec) \t| Clock Ticks"
            "\nBFRULES\t| %d\t| %ld\t| %.2f \t| %.2f \t| %ld\t| %0.4f\t| %.0f\t| %0.4f\t| %.0f"
            ,D1.itemsCount()
            ,D1.transactionCount()
            ,supp_thres
            ,conf_thres
            ,merCount
            ,0.0
            ,0.0
            ,((float)(end-start))/CLOCKS_PER_SEC
            ,(float)(end-start));    
    resultFile = p.postProcess(outfilepath);
    cout<<"\n\nOutput file path: "<<outfilepath<<"\n";    
    printf("\nTime Summary: ");
    printf("\n           ,#Items    ,#DS       ,ParseFile     ,IBMCreate     ,getNextCandidate ,subsetCheck      ,getSupport    ,getNextRule   ,CalcConf      ,");
    printf("\ntime(sec)  ,%-10d,%-10ld,%-14.3f,%-14.3f,%s,%s,%-14.3f,%s,%-14.3f,",
            D1.itemsCount(),
            D1.transactionCount(),
            (float)D1.parseTime/CLOCKS_PER_SEC,
            (float)D1.createIBMTime/CLOCKS_PER_SEC,
            "NA",
            "NA",          
            (float)miner1.timeGetSupportCount/CLOCKS_PER_SEC,
            "NA",
            (float)miner1.timeCalculateConfidence/CLOCKS_PER_SEC);
    printf("\nclk cycles ,%-10d,%-10ld,%-14.0f,%-14.0f,%s,%s,%-14.0f,%s,%-14.0f,",
            D1.itemsCount(),
            D1.transactionCount(),
            (float)D1.parseTime,          
            (float)D1.createIBMTime,
            "NA",
            "NA",
            (float)miner1.timeGetSupportCount,
            "NA",
            (float)miner1.timeCalculateConfidence);
    printf("\nCalls      ,%-10d,%-10ld,%-14d,%-14d,%s,%s,%-14ld,%s,%-14ld,"
            ,D1.itemsCount()
            ,D1.transactionCount()
            ,1
            ,1
            ,"NA"
            ,"NA"
            ,miner1.countGetSupportCount
            ,"NA"
            ,miner1.countCalculateConfidence);    
    printf("\n\n");
    showTop10(resultFile);
    return 0;
}

