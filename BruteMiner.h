/* 
 * File:   BruteMiner.h
 * Author: aeon
 *
 * Created on 7 January, 2013, 10:51 PM
 */

#include "SetOfSet.h"
#include <string>
#include <map>
#include "Dataset.h"

using namespace std;

#ifndef BRUTEMINER_H
#define	BRUTEMINER_H

class BruteMiner {
public:
     private:
        Dataset &ds;        
        SetOfSet Freq;
        float minsup;
        float minconf;
        long int minsupportcount;
        string output_file_path;                                   
        float getSupportCount(set<int> &);                
        long int writeRuleToFile(ofstream& out, set<int> a,set<int> b);
        long int writeRuleToFile(ofstream& out, set<int> a,set<int> b,float confval);                
        bool isIntersection(set<int> &a ,set<int> &b);
        bool isLexicographicallyBefore(set<int> &a ,set<int> &b);
        void display_set(set<int> &x);
    public:
        BruteMiner(Dataset &d);
        long int mine(char  *o_path,float m_sup,float m_conf);             
        void showRule(set<int> &X, set<int> &Y);
        clock_t timeGetSupportCount, timeCalculateConfidence;
        long int countGetSupportCount, countCalculateConfidence;

};

#endif	/* BRUTEMINER_H */

