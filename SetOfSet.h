/* 
 * File:   SetOfSet.h
 * Author: aeon
 *
 * Created on 19 December, 2012, 10:03 PM
 */

#include <set>

using namespace std;

#ifndef SETOFSET_H
#define	SETOFSET_H

class SetOfSet
{
    private:
        set< set<int> > s;
    public:
        SetOfSet(){};        
        virtual ~SetOfSet(){};              
        bool subsetCheck(set<int> &x);
        bool add(set<int>);
        bool isIntersection(const set<int>& a, const set<int>& b);
        long int cardinality(void);
        set< set<int> >::iterator first(void); 
        set< set<int> >::iterator last(void);         
        void display(void);
};






#endif	/* SETOFSET_H */

