/* 
 * File:   BruteMiner.cpp
 * Author: aeon
 * 
 * Created on 7 January, 2013, 10:51 PM
 */

#include "BruteMiner.h"
#include "subsetGen.h"

#include <fstream>
#include <string.h>
#include<iostream>

BruteMiner::BruteMiner(Dataset& d) : ds(d)
{ 
    countGetSupportCount=0;
    countCalculateConfidence=0;
    timeGetSupportCount=0;
    timeCalculateConfidence=0;    
}
    


float BruteMiner::getSupportCount(set<int> &candidate)
{
    countGetSupportCount++;
    float sup=0;
    bool flag;
    set<int>::iterator c_itr;
    for(int i=0;i<ds.no_of_transactions;i++)
    {
        flag=true;
        for(c_itr=candidate.begin();(flag)&&(c_itr!=candidate.end());c_itr++)
        {
            if (ds.ibm[(*c_itr)][i]==true)
            {
                sup++;
                flag=false;
            }
        }
    }
    return (sup/ds.no_of_transactions);
}


bool BruteMiner::isLexicographicallyBefore(set<int> &a ,set<int> &b)
{
    set<int>::iterator itr_a, itr_b;
    for (itr_a=a.begin(),itr_b=b.begin(); ((itr_a!=a.end())&&(itr_b!=b.end()));itr_a++,itr_b++)
    {
        if (*itr_a != *itr_b)
        {
            if (*itr_a < *itr_b)
                return true;
            else
                return false;
            
        }
    }
    return false;
}


bool BruteMiner::isIntersection(set<int> &a ,set<int> &b)
{
    set<int>::iterator itr_a, itr_b;
    for (itr_a=a.begin(),itr_b=b.begin(); ((itr_a!=a.end())&&(itr_b!=b.end()));itr_a++,itr_b++)
    {
        if (*itr_a == *itr_b)
        {
            return true;            
        }
    }
    return false;
}



long int BruteMiner::mine(char * o_path, float m_sup, float m_conf)
{
    clock_t start,end;
    float supp,conf,deno;   
    long int count_rules=0;
    float sup_lhs,sup_rhs;    
    ofstream outfile(o_path,ios::binary);        
    cout<<"\n==========Mining with below parameters==========";
    cout<<"\nSupport Threshold: "<<m_sup;
    cout<<"\nConfidence Threshold: "<<m_conf;  
    subsetGen lhs(ds.items);
    lhs.getNextSubset();    
    while(!(lhs.buf.empty()))
    {                
        sup_lhs=getSupportCount(lhs.buf);
        if (sup_lhs>=m_sup)
        {
            set<int> t=ds.items;         
            set<int>::iterator itr;
            for(itr=lhs.buf.begin();itr!=lhs.buf.end();itr++)
            {
                t.erase(*itr);
            }        
            subsetGen rhs(t);
            rhs.getNextSubset();
            while(!(rhs.buf.empty()))
            {   
//                cout<<"\nTRYING ";
//                display_set(lhs.buf);
//                cout<< " XOR ";
//                display_set(rhs.buf);
                if (rhs.buf.size() <= lhs.buf.size())            
                {
                    if (lhs.buf.size() == rhs.buf.size())
                    {
                        if(!(isLexicographicallyBefore(rhs.buf,lhs.buf)))
                        {
                            sup_rhs=getSupportCount(rhs.buf);
                            if (sup_rhs>=m_sup)
                            {
                                set<int> uni;
                                uni=rhs.buf;
                                set<int>::iterator it;
                                for (it=lhs.buf.begin();it!=lhs.buf.end();it++)
                                {
                                    uni.insert(*it);
                                }
                                deno=getSupportCount(uni);
                                //conf=2 - ((sup_rhs+sup_lhs)/(deno));
                                float supp_lhs_rhs,supp_lhs_minus_rhs,supp_rhs_minus_lhs,conf_lhs_minus_rhs,conf_rhs_minus_lhs;
                                supp_lhs_minus_rhs = deno - sup_rhs;
                                supp_rhs_minus_lhs = deno - sup_lhs;
                                conf_lhs_minus_rhs = supp_lhs_minus_rhs / sup_lhs;
                                conf_rhs_minus_lhs = supp_rhs_minus_lhs / sup_rhs;
                                if (conf_lhs_minus_rhs <= conf_rhs_minus_lhs)
                                    conf= conf_lhs_minus_rhs;
                                else
                                    conf= conf_rhs_minus_lhs;
                                
                                if (conf >= m_conf)
                                {
//                                    display_set(lhs.buf);
//                                    cout<< " XOR ";
//                                    display_set(rhs.buf);
//                                    cout<<"\t"<<sup_lhs<<"\t"<<sup_rhs<<"\t"<<conf;
//                                    cout<<"\n";
                                    writeRuleToFile(outfile,lhs.buf,rhs.buf,conf);
                                    count_rules++;   
                                }
                            }
                        }
                    }
                    else
                    {
                        sup_rhs=getSupportCount(rhs.buf);
                        if (sup_rhs>=m_sup)
                        {
                            set<int> uni;
                            uni=rhs.buf;
                            set<int>::iterator it;
                            for (it=lhs.buf.begin();it!=lhs.buf.end();it++)
                            {
                                uni.insert(*it);
                            }
                            deno=getSupportCount(uni);
                            //conf=2 - ((sup_rhs+sup_lhs)/(deno));
                            float supp_lhs_rhs,supp_lhs_minus_rhs,supp_rhs_minus_lhs,conf_lhs_minus_rhs,conf_rhs_minus_lhs;
                                supp_lhs_minus_rhs = deno - sup_rhs;
                                supp_rhs_minus_lhs = deno - sup_lhs;
                                conf_lhs_minus_rhs = supp_lhs_minus_rhs / sup_lhs;
                                conf_rhs_minus_lhs = supp_rhs_minus_lhs / sup_rhs;
                                if (conf_lhs_minus_rhs <= conf_rhs_minus_lhs)
                                    conf= conf_lhs_minus_rhs;
                                else
                                    conf= conf_rhs_minus_lhs;
                            if (conf >= m_conf)
                            {
//                                display_set(lhs.buf);
//                                cout<< " XOR ";
//                                display_set(rhs.buf);
//                                cout<<"\t"<<sup_lhs<<"\t"<<sup_rhs<<"\t"<<conf;
//                                cout<<"\n";
                                writeRuleToFile(outfile,lhs.buf,rhs.buf,conf);
                                count_rules++;   
                            }
                        }
                    }
                }                                
                rhs.getNextSubset();            
            } 
            
        }
        lhs.getNextSubset();   
    }      
    cout<<"\n\nNumber of mutual exclusion rules: "<<count_rules;
    outfile.flush();
    outfile.close();
    return count_rules;
}
long int BruteMiner::writeRuleToFile(ofstream& out, set<int> a,set<int> b)
{
    set<int>::iterator itr;
    out<<"{";
    for (itr=a.begin();itr!=a.end();itr++)
    {
        out<<(*itr)<<", ";
    }
    out.seekp(-2,ios::cur);
    out<<"} exclusive {";
    for (itr=b.begin();itr!=b.end();itr++)
    {
        out<<(*itr)<<", ";
    }
    out.seekp(-2,ios::cur);
    out<<"}\n";
}



void BruteMiner::showRule(set<int> &X, set<int> &Y)
{
    set<int>::iterator itr;
    cout<<"\nX:";
    for(itr=X.begin();itr!=X.end();itr++)
    {
        cout<<(*itr);
    }            
    cout<<"\tY::";
    for(itr=Y.begin();itr!=Y.end();itr++)
    {
        cout<<(*itr);
    }            

}


void BruteMiner::display_set(set<int> &x)
{
    set<int>::iterator itr;
    cout<<"{";
    for (itr=x.begin();itr!=x.end();itr++)
    {
        cout<<*itr<<", ";
    }    
    cout<<"}";
}

long int BruteMiner::writeRuleToFile(ofstream& out, set<int> a,set<int> b,float confval)
{
    set<int>::iterator itr;
    out<<"{";
    for (itr=a.begin();itr!=a.end();itr++)
    {
        out<<(*itr)<<", ";
    }
    out.seekp(-2,ios::cur);
    out<<"} exclusive {";
    for (itr=b.begin();itr!=b.end();itr++)
    {
        out<<(*itr)<<", ";
    }
    out.seekp(-2,ios::cur);
    out<<"} \t"<<confval<<" \n";
}