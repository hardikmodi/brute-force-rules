#include "Dataset.h"
#include <iostream>
#include <fstream>
#include <string.h>
#include <sstream>
#include <cstdlib>

using namespace std;

#define buffer_size 1024*1024*1024
#define line_size 2*1024

Dataset::Dataset(const char *ds_file_path, char delim = ',') 
{
    delimiter = delim;
    path = new char[strlen(ds_file_path)];
    strcpy(path,ds_file_path);
    parseTime=clock();
    parseDataset();
    parseTime = clock() - parseTime;    
    createIBMTime = clock();
    createIBM();    
    createIBMTime = clock()-createIBMTime;
}
        


void Dataset::parseDataset()
{
    istringstream strstream;
    ifstream ds_file;
    
    ds_file.open(path, std::ifstream::in);
    if (ds_file.is_open())
    {
        char *buf = new char[buffer_size];
        char *line = new char[line_size];
        long int T=0;
        while (!(ds_file.eof()))
        {
            ds_file.read(buf,buffer_size);
            strstream.rdbuf()->pubsetbuf(buf,buffer_size);
            while (strstream.good())
            {
                strstream.getline(line,line_size,'\n');                       
                int sum=0;
                for (int i=0;i<strlen(line);i++)
                {
                    if (line[i]==delimiter)
                    {
                        items.insert(sum);
                        sum=0;
                    }
                    else
                        sum = (sum*10)+(line[i]-'0');                
                }
                if (strlen(line)>0)
                    T++;
           }
        }
        no_of_transactions = T;
        no_of_items=items.size();
        delete[] buf;
        delete[] line;
        ds_file.close();
    }
    else
    {
        cout<<"\nCould not open file\n";
        exit(1);
    }
}

void Dataset::createIBM(void)
{
    //allocating memory for bit matrix;
    try
    {
        ibm = new bool*[no_of_items];       
        for (long int i=0;i<no_of_items;i++)
        {
            ibm[i]= new bool[no_of_transactions];
        }
    }
    catch (bad_alloc)
    {
        cout<<"not Sufficient Memory";
    }
    for (long int i = 0;i<no_of_items;i++)
    {
        for (int j=0;j<no_of_transactions;j++)
        {
            ibm[i][j]=false;
        }
    }
    ifstream ds_file;
    ds_file.open(path,std::ios::in | std::ios::binary);
    istringstream strstream;
    char *buf = new char[buffer_size];
    char *line = new char[line_size];    
    long int T=0;
    while (!(ds_file.eof()))
    {
        ds_file.read(buf,buffer_size);
        strstream.rdbuf()->pubsetbuf(buf,buffer_size);        
        while (strstream.good())
        {
            strstream.getline(line,line_size,'\n');           
            int sum=0;
            for(int i=0;i<strlen(line);i++)
            {
                if (line[i]==delimiter)
                {
                    ibm[sum][T]=true;                    
                    sum=0;
                }
                else
                    sum=(sum*10)+(line[i]-'0');
            }
            if (strlen(line)>0)
                T++;
        }
    }
    ds_file.close();
    delete [] buf;
    delete [] line;
            
}

void Dataset::summarize(void)
{
    cout<<"\n\n=============Properties of Dataset============="<<endl;
    cout<<"Location of File ::"<<path<<endl;
    cout<<"Delimiter:: |"<<delimiter<<"|"<<endl;
    cout<<"Number of Transactions:: "<<no_of_transactions<<endl;    
    cout<<"Items Encountered ::"<<no_of_items<<endl;
    set<int>::iterator itr;   
}
Dataset::~Dataset()
{
    for (long int i=0;i<no_of_items;i++)
    {
        delete [] ibm[i];        
    }
    delete [] ibm;
    delete [] path;
}


int Dataset::itemsCount(void)
{
    return no_of_items;
}
 
long int Dataset::transactionCount(void)
{
    return no_of_transactions;
}